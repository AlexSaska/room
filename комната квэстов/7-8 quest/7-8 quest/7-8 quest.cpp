﻿/*
 * _7_8_quest.cpp
 *
 * Created: 09.08.2015 21:42:56
 *  Author: Alexander Shaburov
 * 7 - 8 квэст
 */ 

#include <avr/io.h>
#include <math.h>
#include <util/delay.h>

/*датчики*/
#define sensor_dvd1 PD0
#define sensor_dvd2 PD1
#define sensor_dvd3 PD2
#define sensor_dvd4 PD3
#define sensor_dvd5 PD4

/*количество игроков*/
#define game2 PC0
#define game3 PC1
#define game4 PC2
#define game5 PC3

/*джэки*/
#define j1 PD5
#define j2 PD6
#define j3 PD7
#define j4 PB0
#define j5 PB1

/*проверка джэков*/
#define j_1 PB2
#define j_2 PB3
#define j_3 PB4
#define j_4 PB5
#define j_5 PB6

/*выход*/
#define win PB7 

/*если диск или нету*/
bool dvd1 = false;
bool dvd2 = false;
bool dvd3 = false;
bool dvd4 = false;
bool dvd5 = false;

uint8_t dvdv_disk[5] = {0,0,0,0,0};
/*выходы*/

static uint8_t stat = 0;

/*кол-во учатсников*/
uint8_t number_player;

/*таймеры*/
uint8_t time_dvd1 = 0;
uint8_t time_dvd2 = 0;
uint8_t time_dvd3 = 0;
uint8_t time_dvd4 = 0;
uint8_t time_dvd5 = 0;

int main(void)
{
	DDRD |= (0<<PD0) | (0<<PD1) | (0<<PD2) | (0<<PD3) | (0<<PD4) | (1 << PD5) | (1 <<PD6) | (1<<PD7);
	PORTD |= (1<<PD0) | (1<<PD1) | (1<<PD2) | (1<<PD3) | (1<<PD4) | (0<<PD5) | (0<<PD6) | (0<<PD7);
	DDRC|= (0<<PC0) | (0<<PC1) | (0<<PC2) | (0<<PC3);
	PORTC |= (1<<PC0) | (1<<PC1) | (1<<PC2) | (1<<PC3);		
	DDRB |= (1<<PB0) | (1<<PB1);
	PORTB |= (0<<PB0) | (0<<PB1);
    while(1)
    {
        switch(stat){
			/*кол-во участников*/
			case 0:
				if((PINC & (1<<game2))==0){
					number_player = 2;
					stat = 1;
				}
				if((PINC & (1<<game3))==0){
					number_player = 3;
					stat = 1;
				}
				if((PINC & (1<<game4))==0){
					number_player = 4;
					stat = 1;
				}
				if((PINC & (1<<game5))==0){
					number_player = 5;
					stat = 1;
				}
			break;
			/*определяем диски в двдиромах*/
			case 1:		
				if(number_player == 2){
					PORTD |= 1<<j3;
					while(((PIND & (1<<sensor_dvd1))==0) && ((PIND & (1<<sensor_dvd2))==0) ){
						time_dvd1++;
						_delay_ms(100);
						if(time_dvd1 > 80){
							PORTD |= (1<< j1) | (1<< j2);
							dvd1 = true;
							dvd2 = true;
							stat = 2;							
						}
					}														
				}		
			break;
			/*проверка джэков*/
			case 2:
				if(number_player == 2){
				}					
			break;
		}			
	}						
}