﻿/*
 * _2_quest.cpp
 *
 * Created: 11.08.2015 11:35:22
 *  Author: USER
 */ 

#include <avr/io.h>
#include "math.h"
#include <util/delay.h>
#include  <avr/interrupt.h>

/*читаем кнопки*/
void button_read(void);
void init_gpio(void);
void init_spi(void);
void init_timer(void);
void send_spi(uint8_t number1, uint8_t number2);

/*произведения правильных комбинаций *10 */
#define comb1 60
#define comb2 2400
#define comb3 11880
#define comb4 2800

/*кол-во нажатий при выборе комбинации*/
uint8_t press_1 = 0;
uint8_t press_2 = 0;
uint8_t press_3 = 0;
uint8_t press_4 = 0;

uint8_t data;

/* массивы для значений*/
uint8_t c1[3];
uint8_t c2[3];
uint8_t c3[3];
uint8_t c4[3];

/*посчитаем*/
uint16_t d1;
uint16_t d2;
uint16_t d3;
uint16_t d4;

/*кнопки*/
#define led1 PD0
#define led2 PD1
#define led3 PD2
#define led4 PD3
#define led5 PD4
#define led6 PD5
#define led7 PD6
#define led8 PD7
#define led9 PB0
#define led10 PB1
#define led11 PB2
#define led12 PC0

/*выход*/
#define win PC1

/* в спи*/
volatile uint8_t n1 = 0b00000000;
volatile uint8_t n2 = 0b00000000;

ISR(TIMER0_OVF_vect){
  send_spi(n1,n2); 

}

void button_read(void){
	if((PIND & (1<<led1)) == 0){
		data = 1;
		n1 |= (1<<7);
		_delay_ms(100);
	}
	if((PIND & (1<<led2)) == 0){
		data = 2;
		n1 |= (1<<6);
		_delay_ms(100);
	}
	if((PIND & (1<<led3)) == 0){
		data = 3;
		n1 |= (1<<5);
		_delay_ms(100);
	}
	if((PIND & (1<<led4)) == 0){
		data = 4;
		n1 |= (1<<4);
		_delay_ms(100);
	}
	if((PIND & (1<<led5)) == 0){
		data = 5;
		n1 |= (1<<3);
		_delay_ms(100);
	}
	if((PIND & (1<<led6)) == 0){
		data = 6;
		n1 |= (1<<2);
		_delay_ms(100);
	}		
	if((PIND & (1<<led7)) == 0){
		data = 7;
		n1 |= (1<<1);
		_delay_ms(100);
	}
	if((PIND & (1<<led8)) == 0){
		data = 8;
		n1 |= (1<<0);
		_delay_ms(100);
	}
	if((PINB & (1<<led9)) == 0){
		data = 9;
		n2 |= (1<<7);
		_delay_ms(100);
	}
	if((PINB & (1<<led10)) == 0){
		data = 10;
		n2 |= (1<<6);
		_delay_ms(100);
	}
	if((PINB & (1<<led11)) == 0){
		data = 11;
		n2 |= (1<<5);
		_delay_ms(100);
	}
	if((PINC & (1<<led12)) == 0){
		data = 12;
		n2 |= (1<<4);
		_delay_ms(100);
	}
}

void init_gpio(void){
	DDRD |= 0x00;
	PORTD |= 0xff;
	DDRB |= (0<<0) | (0<<1) | (0<<2) | (1<<3) | (1<<4) | (1<<5);
	PORTB |= (1<<0) | (1<<1) | (1<<2);
	DDRC |= (0<<0) | (1<<1) | (1<<2);
	PORTC |= (1<<0) | (0<<1);
}

void init_spi(void){
	SPCR = 0b01110011;
}

void init_timer(void){
	 TCCR0 |= 1<<CS01;
	 TCNT0 = 0xff;
	 TIMSK |= 1<<TOIE0;
}

void send_spi(uint8_t number1, uint8_t number2){
	PORTC &= ~(1<<PC2);
	SPDR = number2;
	while(!(SPSR &(1<<SPIF)));
	_delay_ms(1);
	SPDR = number1;
	while(!(SPSR &(1<<SPIF)));
	_delay_ms(1);
	PORTC |= (1<<PC2);
}

int main(void)
{
	cli();
	init_gpio();
	init_spi();
	init_timer();
	sei();
    while(1)
    {
        uint8_t status = 0;
		switch (status)
		{
			/*выбор первой комбинации*/
			case 0:
				button_read();
			break;
			/*выбор второй комбинации*/
			case 1:
				;
			break;
		}
    }
}