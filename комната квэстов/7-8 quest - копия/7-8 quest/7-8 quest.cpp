﻿/*
 * _7_8_quest.cpp
 *
 * Created: 09.08.2015 21:42:56
 *  Author: Alexander Shaburov
 * 7 - 8 квэст
 */ 

#include <avr/io.h>
#include <math.h>
#include <util/delay.h>

/*датчики*/
#define sensor_dvd1 PD0
#define sensor_dvd2 PD1
#define sensor_dvd3 PD2
#define sensor_dvd4 PD3
#define sensor_dvd5 PD4

/*количество игроков*/
#define game2 PC0
#define game3 PC1
#define game4 PC2
#define game5 PC3

/*джэки*/
#define j1 PD5
#define j2 PD6
#define j3 PD7
#define j4 PB0
#define j5 PB1

/*проверка джэков*/
#define j_1 PB2
#define j_2 PB3
#define j_3 PB4
#define j_4 PB5
#define j_5 PB6

/*выход*/
#define win PB7 

/*если диск или нету*/
bool dvd1 = false;
bool dvd2 = false;
bool dvd3 = false;
bool dvd4 = false;
bool dvd5 = false;

uint8_t dvdv_disk[5] = {0,0,0,0,0};
/*выходы*/

static uint8_t stat = 0;

/*кол-во учатсников*/
uint8_t number_player;

/*таймеры*/
uint8_t time_dvd1 = 0;
uint8_t time_dvd2 = 0;
uint8_t time_dvd3 = 0;
uint8_t time_dvd4 = 0;
uint8_t time_dvd5 = 0;

int main(void)
{
	DDRD |= (0<<PD0) | (0<<PD1) | (0<<PD2) | (0<<PD3) | (0<<PD4) | (1 << PD5) | (1 <<PD6) | (1<<PD7);
	PORTD |= (1<<PD0) | (1<<PD1) | (1<<PD2) | (1<<PD3) | (1<<PD4) | (0<<PD5) | (0<<PD6) | (0<<PD7);
	DDRC|= (0<<PC0) | (0<<PC1) | (0<<PC2) | (0<<PC3);
	PORTC |= (1<<PC0) | (1<<PC1) | (1<<PC2) | (1<<PC3);		
	DDRB |= (1<<PB0) | (1<<PB1) | (0<<PB2) | (0<<PB3) | (0<<PB4) |  (0<<PB5) | (0<<PB6) | (1<<PB7);
	PORTB |= (0<<PB0) | (0<<PB1) | (1<<PB2) | (1<<PB3) | (1<<PB4) |  (1<<PB5) | (1<<PB6) | (0<<PB7);
    while(1)
    {
        switch(stat){
			/*кол-во участников*/
			case 0:
				if((PINC & (1<<game2))==0){
					number_player = 2;
					stat = 1;
				}
				if((PINC & (1<<game3))==0){
					number_player = 3;
					stat = 1;
				}
				if((PINC & (1<<game4))==0){
					number_player = 4;
					stat = 1;
				}
				if((PINC & (1<<game5))==0){
					number_player = 5;
					stat = 1;
				}
			break;
			/*определяем диски в двдиромах*/
			case 1:		
			///////////////////////////////////////////////////////////////////////
			//////////////////////////2игрока			
				if(number_player == 2){					
					if(((PIND & (1<<sensor_dvd1))==0) && (time_dvd1<40)){
						time_dvd1++;
						_delay_ms(100);
						if(time_dvd1 > 35){
							PORTD |= (1<< j1);
							dvd1 = true;																		
						}					
					}		
					if(((PIND & (1<<sensor_dvd2))==0) && time_dvd2 < 40) {
						time_dvd2++;
						_delay_ms(100);
						if(time_dvd2 > 35){
							PORTD |= (1<< j2);							
							dvd2 = true;													
						}						
					}
					if (dvd1 && dvd2)
					{
						stat = 2;
					}
																																									
				}
				///////////////////////////////////////////////////////////////////////
				//////////////////////////3игрока
					if(number_player == 3){					
					if(((PIND & (1<<sensor_dvd1))==0) && (time_dvd1<40)){
						time_dvd1++;
						_delay_ms(100);
						if(time_dvd1 > 35){
							PORTD |= (1<< j1);
							dvd1 = true;																		
						}					
					}		
					if(((PIND & (1<<sensor_dvd2))==0) && time_dvd2 < 40) {
						time_dvd2++;
						_delay_ms(100);
						if(time_dvd2 > 35){
							PORTD |= (1<< j2);							
							dvd2 = true;													
						}						
					}
					if(((PIND & (1<<sensor_dvd3))==0) && time_dvd3 < 40) {
						time_dvd3++;
						_delay_ms(100);
						if(time_dvd3 > 35){
							PORTD |= (1<< j3);							
							dvd3 = true;													
						}						
					}
					if (dvd1 && dvd2 && dvd3)
					{
						stat = 2;
					}																																									
				}
				///////////////////////////////////////////////////////////////////////
				//////////////////////////4игрока
					if(number_player == 4){					
					if(((PIND & (1<<sensor_dvd1))==0) && (time_dvd1<40)){
						time_dvd1++;
						_delay_ms(100);
						if(time_dvd1 > 35){
							PORTD |= (1<< j1);
							dvd1 = true;																		
						}					
					}		
					if(((PIND & (1<<sensor_dvd2))==0) && time_dvd2 < 40) {
						time_dvd2++;
						_delay_ms(100);
						if(time_dvd2 > 35){
							PORTD |= (1<< j2);							
							dvd2 = true;													
						}						
					}
					if(((PIND & (1<<sensor_dvd3))==0) && time_dvd3 < 40) {
						time_dvd3++;
						_delay_ms(100);
						if(time_dvd3 > 35){
							PORTD |= (1<< j3);							
							dvd3 = true;													
						}						
					}
					if(((PIND & (1<<sensor_dvd4))==0) && time_dvd4 < 40) {
						time_dvd4++;
						_delay_ms(100);
						if(time_dvd4 > 35){
							PORTB |= (1<< j4);							
							dvd4 = true;													
						}						
					}
					if (dvd1 && dvd2 && dvd3 && dvd4)
					{
						stat = 2;
					}																																									
				}	
				///////////////////////////////////////////////////////////////////////
				//////////////////////////5игрока
					if(number_player == 5){					
					if(((PIND & (1<<sensor_dvd1))==0) && (time_dvd1<40)){
						time_dvd1++;
						_delay_ms(100);
						if(time_dvd1 > 35){
							PORTD |= (1<< j1);
							dvd1 = true;																		
						}					
					}		
					if(((PIND & (1<<sensor_dvd2))==0) && time_dvd2 < 40) {
						time_dvd2++;
						_delay_ms(100);
						if(time_dvd2 > 35){
							PORTD |= (1<< j2);							
							dvd2 = true;													
						}						
					}
					if(((PIND & (1<<sensor_dvd3))==0) && time_dvd3 < 40) {
						time_dvd3++;
						_delay_ms(100);
						if(time_dvd3 > 35){
							PORTD |= (1<< j3);							
							dvd3 = true;													
						}						
					}
					if(((PIND & (1<<sensor_dvd4))==0) && time_dvd4 < 40) {
						time_dvd4++;
						_delay_ms(100);
						if(time_dvd4 > 35){
							PORTB |= (1<< j4);							
							dvd4 = true;													
						}						
					}
					if(((PIND & (1<<sensor_dvd5))==0) && time_dvd5 < 40) {
						time_dvd5++;
						_delay_ms(100);
						if(time_dvd5 > 35){
							PORTB |= (1<< j5);							
							dvd5 = true;													
						}						
					}
					if (dvd1 && dvd2 && dvd3 && dvd4 && dvd5)
					{
						stat = 2;
					}
																																									
				}			
			break;
			case 2:
				if (number_player == 2)				{
					if(((PINB & (1<<j_1))==0)&&((PINB & (1<<j_2))==0)){
						PORTB |= 1<<win;
					}
				}
				if (number_player == 3)				{
					if(((PINB & (1<<j_1))==0)&&((PINB & (1<<j_2))==0)&&((PINB & (1<<j_3))==0)){
						PORTB |= 1<<win;
					}
				}
				if (number_player == 4)				{
					if(((PINB & (1<<j_1))==0)&&((PINB & (1<<j_2))==0)&&((PINB & (1<<j_3))==0)&&((PINB & (1<<j_4))==0)){
						PORTB |= 1<<win;
					}
				}
				if (number_player == 5)				{
					if(((PINB & (1<<j_1))==0)&&((PINB & (1<<j_2))==0)&&((PINB & (1<<j_3))==0)&&((PINB & (1<<j_4))==0)&&((PINB & (1<<j_5))==0)){
						PORTB |= 1<<win;
					}
				}
			break;		
		}			
	}						
}